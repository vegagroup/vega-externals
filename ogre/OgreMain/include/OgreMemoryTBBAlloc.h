#ifndef __MemoryTBBAlloc_H__
#define __MemoryTBBAlloc_H__

#include "OgreAlignedAllocator.h"
#include "OgreMemoryTracker.h"
#include "OgreHeaderPrefix.h"

namespace Ogre
{
#if OGRE_MEMORY_ALLOCATOR == OGRE_MEMORY_ALLOCATOR_TBB

	/** \addtogroup Core
	*  @{
	*/
	/** \addtogroup Memory
	*  @{
	*/

	class _OgreExport TBBAllocImpl
	{
	public:
		static void* allocBytes(size_t count, 
			const char* file, int line, const char* func);
		static void deallocBytes(void* ptr);
		static void* allocBytesAligned(size_t align, size_t count, 
			const char* file, int line, const char* func);
		static void deallocBytesAligned(size_t align, void* ptr);
	};

	/**	A "standard" allocation policy for use with AllocatedObject and 
		TBBAllocator. This is the class that actually does the allocation
		and deallocation of physical memory, and is what you will want to 
		provide a custom version of if you wish to change how memory is allocated.
		@par
		This class just delegates to the global malloc/free.
	*/
	class _OgreExport TBBAllocPolicy
	{
	public:
		static inline void* allocateBytes(size_t count, const char* file = 0, int line = 0, const char* func = 0 )
		{
			void* ptr = TBBAllocImpl::allocBytes(count, file, line, func);
			return ptr;
		}

		static inline void deallocateBytes(void* ptr)
		{
			TBBAllocImpl::deallocBytes( ptr );
		}

		/// Get the maximum size of a single allocation
		static inline size_t getMaxAllocationSize()
		{
			return std::numeric_limits<size_t>::max();
		}
	private:
		// no instantiation
		TBBAllocPolicy()
		{ }
	};

	/**	A "standard" allocation policy for use with AllocatedObject and 
		STLAllocator, which aligns memory at a given boundary (which should be
		a power of 2). This is the class that actually does the allocation
		and deallocation of physical memory, and is what you will want to 
		provide a custom version of if you wish to change how memory is allocated.
		@par
		This class just delegates to the global malloc/free, via AlignedMemory.
		@note
		template parameter Alignment equal to zero means use default
		platform dependent alignment.

	*/
	template <size_t Alignment = 0>
	class TBBAlignedAllocPolicy
	{
	public:
		// compile-time check alignment is available.
		typedef int IsValidAlignment
			[Alignment <= 128 && ((Alignment & (Alignment-1)) == 0) ? +1 : -1];

		static inline void* allocateBytes(size_t count, const char* file = 0, int line = 0, const char* func = 0)
		{
			void* ptr = TBBAllocImpl::allocBytesAligned(Alignment, count, file, line, func);
			return ptr;
		}

		static inline void deallocateBytes(void* ptr)
		{
			TBBAllocImpl::deallocBytesAligned(Alignment, ptr);
		}

		/// Get the maximum size of a single allocation
		static inline size_t getMaxAllocationSize()
		{
			return std::numeric_limits<size_t>::max();
		}
	private:
		// No instantiation
		TBBAlignedAllocPolicy()
		{ }
	};

#endif
	/** @} */
	/** @} */

}// namespace Ogre


#include "OgreHeaderSuffix.h"

#endif // __MemoryStdAlloc_H__
