#include "OgreStableHeaders.h"
#ifdef __ANDROID__
#include "OgreFileSystemLayer.h"

namespace Ogre
{
	void FileSystemLayer::getConfigPaths()
	{
	}
    //---------------------------------------------------------------------
	void FileSystemLayer::prepareUserHome(const Ogre::String& subdir)
	{
	}
    //---------------------------------------------------------------------
	bool FileSystemLayer::fileExists(const Ogre::String& path) const
	{
		return access(path.c_str(), 00) == 0;
	}
    //---------------------------------------------------------------------
	bool FileSystemLayer::createDirectory(const Ogre::String& path)
	{
		return false;
	}
}
#endif